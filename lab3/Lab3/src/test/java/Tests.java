import com.company.FuncSetUtils;
import com.company.PurelyFunctionalSet;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Tests {

    @Test
    public void emptySetTest() {
        PurelyFunctionalSet<Integer> empty = FuncSetUtils.empty();
        for (int i = -1000; i <= 1000; i++)
        {
            assertFalse(empty.contains(i));
        }
    }

    @Test
    public void singletonSetTest() {
        PurelyFunctionalSet<Integer> set1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<String> set2 = FuncSetUtils.singletonSet("Satan");
        PurelyFunctionalSet<Character> set3 = FuncSetUtils.singletonSet('S');

        assertTrue(set1.contains(666));
        assertTrue(set2.contains("Satan"));
        assertTrue(set3.contains('S'));
        assertFalse(set1.contains(333));
        assertFalse(set2.contains("Not Satan"));
        assertFalse(set3.contains('N'));
    }

    @Test
    public void unionTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet2);

        assertTrue(intUnion.contains(666) && intUnion.contains(999));
        assertFalse(intUnion.contains(333));

        PurelyFunctionalSet<String> strSet1 = FuncSetUtils.singletonSet("Satan");
        PurelyFunctionalSet<String> strSet2 = FuncSetUtils.singletonSet("Antichrist");
        PurelyFunctionalSet<String> strUnion = FuncSetUtils.union(strSet1, strSet2);

        assertTrue(strUnion.contains("Satan") && strUnion.contains("Antichrist"));
        assertFalse(strUnion.contains("Not Satan"));

        PurelyFunctionalSet<Character> charSet1 = FuncSetUtils.singletonSet('S');
        PurelyFunctionalSet<Character> charSet2 = FuncSetUtils.singletonSet('A');
        PurelyFunctionalSet<Character> charUnion = FuncSetUtils.union(charSet1, charSet2);

        assertTrue(charUnion.contains('S') && charUnion.contains('A'));
        assertFalse(charUnion.contains('N'));
    }

    @Test
    public void intersectTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet2);
        PurelyFunctionalSet<Integer> intersectIntSet = FuncSetUtils.intersect(intUnion, intSet1);

        assertTrue(intersectIntSet.contains(666));
        assertFalse(intersectIntSet.contains(999));

        PurelyFunctionalSet<String> strSet1 = FuncSetUtils.singletonSet("Satan");
        PurelyFunctionalSet<String> strSet2 = FuncSetUtils.singletonSet("Antichrist");
        PurelyFunctionalSet<String> strUnion = FuncSetUtils.union(strSet1, strSet2);
        PurelyFunctionalSet<String> intersectStrSet = FuncSetUtils.intersect(strUnion, strSet1);

        assertTrue(intersectStrSet.contains("Satan"));
        assertFalse(intersectStrSet.contains("Antichrist"));

        PurelyFunctionalSet<Character> charSet1 = FuncSetUtils.singletonSet('S');
        PurelyFunctionalSet<Character> charSet2 = FuncSetUtils.singletonSet('A');
        PurelyFunctionalSet<Character> charUnion = FuncSetUtils.union(charSet1, charSet2);
        PurelyFunctionalSet<Character> intersectCharSet = FuncSetUtils.intersect(charUnion, charSet1);

        assertTrue(intersectCharSet.contains('S'));
        assertFalse(intersectCharSet.contains('A'));
    }

    @Test
    public void diffTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet2);
        PurelyFunctionalSet<Integer> intersectIntSet = FuncSetUtils.diff(intUnion, intSet1);

        assertTrue(intersectIntSet.contains(999));
        assertFalse(intersectIntSet.contains(666));

        PurelyFunctionalSet<String> strSet1 = FuncSetUtils.singletonSet("Satan");
        PurelyFunctionalSet<String> strSet2 = FuncSetUtils.singletonSet("Antichrist");
        PurelyFunctionalSet<String> strUnion = FuncSetUtils.union(strSet1, strSet2);
        PurelyFunctionalSet<String> intersectStrSet = FuncSetUtils.diff(strUnion, strSet1);

        assertTrue(intersectStrSet.contains("Antichrist"));
        assertFalse(intersectStrSet.contains("Satan"));

        PurelyFunctionalSet<Character> charSet1 = FuncSetUtils.singletonSet('S');
        PurelyFunctionalSet<Character> charSet2 = FuncSetUtils.singletonSet('A');
        PurelyFunctionalSet<Character> charUnion = FuncSetUtils.union(charSet1, charSet2);
        PurelyFunctionalSet<Character> intersectCharSet = FuncSetUtils.diff(charUnion, charSet1);

        assertTrue(intersectCharSet.contains('A'));
        assertFalse(intersectCharSet.contains('S'));
    }

    @Test
    public void filterTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intSet3 = FuncSetUtils.singletonSet(6);
        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet2);
        intUnion = FuncSetUtils.union(intUnion, intSet3);
        PurelyFunctionalSet<Integer> filterIntSet = FuncSetUtils.filter(intUnion, x -> x % 6 == 0);

        assertTrue(filterIntSet.contains(666) && filterIntSet.contains(6));
        assertFalse(filterIntSet.contains(999));

        PurelyFunctionalSet<String> strSet1 = FuncSetUtils.singletonSet("Satan");
        PurelyFunctionalSet<String> strSet2 = FuncSetUtils.singletonSet("Antichrist");
        PurelyFunctionalSet<String> strSet3 = FuncSetUtils.singletonSet("Satanist");
        PurelyFunctionalSet<String> strUnion = FuncSetUtils.union(strSet1, strSet2);
        strUnion = FuncSetUtils.union(strUnion, strSet3);
        PurelyFunctionalSet<String> filterStrSet = FuncSetUtils.filter(strUnion, x -> x.indexOf("Satan") >= 0);

        assertTrue(filterStrSet.contains("Satan") && filterStrSet.contains("Satanist"));
        assertFalse(filterStrSet.contains("Antichrist"));
    }

    @Test
    public void forallTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intSet3 = FuncSetUtils.singletonSet(6);

        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet3);
        assertTrue(FuncSetUtils.forall(intUnion, x -> x % 6 == 0));

        intUnion = FuncSetUtils.union(intUnion, intSet2);
        assertFalse(FuncSetUtils.forall(intUnion, x -> x % 6 == 0));
    }

    @Test
    public void existTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intSet3 = FuncSetUtils.singletonSet(6);

        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet3);
        assertFalse(FuncSetUtils.exists(intUnion, x -> x % 999 == 0));

        intUnion = FuncSetUtils.union(intUnion, intSet2);
        assertTrue(FuncSetUtils.exists(intUnion, x -> x % 999 == 0));
    }

    @Test
    public void mapTest() {
        PurelyFunctionalSet<Integer> intSet1 = FuncSetUtils.singletonSet(666);
        PurelyFunctionalSet<Integer> intSet2 = FuncSetUtils.singletonSet(999);
        PurelyFunctionalSet<Integer> intSet3 = FuncSetUtils.singletonSet(6);
        PurelyFunctionalSet<Integer> intUnion = FuncSetUtils.union(intSet1, intSet3);
        intUnion = FuncSetUtils.union(intUnion, intSet2);
        PurelyFunctionalSet<Integer> mapSet = FuncSetUtils.map(intUnion, x -> x / 3);

        assertTrue(mapSet.contains(222) && mapSet.contains(333) && mapSet.contains(2));
    }
}