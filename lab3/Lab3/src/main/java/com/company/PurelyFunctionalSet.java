package com.company;

public interface PurelyFunctionalSet<T> {
    boolean contains(T Element);
}

