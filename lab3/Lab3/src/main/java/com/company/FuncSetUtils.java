package com.company;

import java.util.function.Function;

public class FuncSetUtils {
    public static<T>PurelyFunctionalSet<T> empty() {
        return x -> false;
    }

    public static <T>PurelyFunctionalSet<T> singletonSet(T val) {
        return x -> x.equals(val);
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> s, PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) || t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> s, PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> s, PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && !t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> s, Predicate<T> p) {
        return x -> s.contains(x) && p.test(x);
    }

    private static boolean AllElementsRecursion(PurelyFunctionalSet<Integer> s, int iterator, Predicate<Integer> p, boolean exists) {
        if(iterator > 1000) return !exists;
        else if(s.contains(iterator) && p.test(iterator) == exists)
            return exists;
        return AllElementsRecursion(s, ++iterator, p, exists);
    }

    public static <T> boolean forall(PurelyFunctionalSet<Integer> s, Predicate<Integer> p) {
        return AllElementsRecursion(s, -1000, p, false);
    }

    public static <T> boolean exists(PurelyFunctionalSet<Integer> s, Predicate<Integer> p) {
        return AllElementsRecursion(s, -1000, p, true);
    }

    private static <R> PurelyFunctionalSet<Integer> mapTailRecursive(PurelyFunctionalSet<Integer> s, int iterator, Function<Integer, R> p, PurelyFunctionalSet<Integer> acc) {
        if(iterator > 1000) return acc;
        else if(s.contains(iterator))
        {
            PurelyFunctionalSet<Integer> intermRes = union(acc, singletonSet((Integer) p.apply(iterator)));
            return mapTailRecursive(s, ++iterator, p, intermRes);
        }
        return mapTailRecursive(s, ++iterator, p, acc);
    }

    public static <T, R> PurelyFunctionalSet<Integer> map(PurelyFunctionalSet<Integer> s, Function<Integer, R> p) {
        return mapTailRecursive(s, -1000, p, empty());
    }
}
