package com.company;

public class Circle {
    double r;
    double x;
    double y;

    public Circle(double r,double x, double y)
    {
        this.r = r;
        this.x = x;
        this.y = y;
    }

    public void showInfo()
    {
        System.out.println("Radius:" + r +"\nCoordinates: x=" + x + ", y=" + y);
    }

    public double Square()
    {
        return Math.PI*r*r;
    }
}
