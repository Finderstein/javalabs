package com.company;

public class Main {
    public static void main(String[] args)
    {
        Circle c1 = new Circle(10, 3,4);
        Circle c2 = new Circle(2.8, 8,4);
        Circle c3 = new Circle(7.3, 7.1,8.9);

        System.out.println("MAVEN LIBRARY");
        System.out.println("Circle 1:");
        c1.showInfo();
        System.out.println("Square:" + c1.Square());
        System.out.println("***************");

        System.out.println("Circle 2:");
        c2.showInfo();
        System.out.println("Square:" + c2.Square());
        System.out.println("***************");

        System.out.println("Circle 3:");
        c3.showInfo();
        System.out.println("Square:" + c3.Square());
        System.out.println("########################");

        GradleCircle gc1 = new GradleCircle(3, 1,1);
        GradleCircle gc2 = new GradleCircle(7, 2.4,0);
        GradleCircle gc3 = new GradleCircle(5.3, 99,101.9);

        System.out.println("GRADLE LIBRARY");
        System.out.println("GradleCircle 1:");
        gc1.showInfo();
        System.out.println("Square:" + gc1.Square());
        System.out.println("***************");

        System.out.println("GradleCircle 2:");
        gc2.showInfo();
        System.out.println("Square:" + gc2.Square());
        System.out.println("***************");

        System.out.println("GradleCircle 3:");
        gc3.showInfo();
        System.out.println("Square:" + gc3.Square());
    }
}
