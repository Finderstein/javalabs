package com.company.dao;

import com.company.model.Author;
import com.company.model.Drawing;
import com.company.model.Photo;
import com.company.model.Picture;

import java.util.List;

public interface IDAO {
    Author getAuthor(Long id);
    List<Author> getAuthorList();

    Picture getPicture(Long id);
    List<Picture> getPictureList();

    Photo getPhoto(Long id);
    List<Photo> getPhotoList();

    Drawing getDrawing(Long id);
    List<Drawing> getDrawingList();
}
