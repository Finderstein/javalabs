package com.company.dao;

import com.company.model.Author;
import com.company.model.Drawing;
import com.company.model.Photo;
import com.company.model.Picture;

import java.util.List;

public class DAO implements IDAO {
    DAOImpl<Author> authorsDaoImpl = new DAOImpl<Author>(Author.class);
    DAOImpl<Picture> pictureIDAOImpl = new DAOImpl<Picture>(Picture.class);
    DAOImpl<Photo> photoIDAOImpl = new DAOImpl<Photo>(Photo.class);
    DAOImpl<Drawing> drawingIDAOImpl = new DAOImpl<Drawing>(Drawing.class);

    @Override
    public Author getAuthor(Long id) {
        return authorsDaoImpl.getEntity(id);
    }

    @Override
    public List<Author> getAuthorList() {
        return authorsDaoImpl.getEntityList();
    }

    @Override
    public Picture getPicture(Long id) {
        return pictureIDAOImpl.getEntity(id);
    }

    @Override
    public List<Picture> getPictureList() {
        return pictureIDAOImpl.getEntityList();
    }

    @Override
    public Photo getPhoto(Long id) {
        return photoIDAOImpl.getEntity(id);
    }

    @Override
    public List<Photo> getPhotoList() {
        return photoIDAOImpl.getEntityList();
    }

    @Override
    public Drawing getDrawing(Long id) {
        return drawingIDAOImpl.getEntity(id);
    }

    @Override
    public List<Drawing> getDrawingList() {
        return drawingIDAOImpl.getEntityList();
    }
}
