package com.company.dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DAOImpl<T> implements IDAOImpl<T> {
    PreparedStatement statement;
    Connection connection;

    Class<T> clazz;

    public DAOImpl(Class<T> clazz) {
        this.clazz = clazz;
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        return fields;
    }

    private static Object toObject( Class c, String value ) {
        if( Boolean.class == c || Boolean.TYPE == c)
            return value.equals("t");
        if( Byte.class == c || Boolean.TYPE == c) return Byte.parseByte( value );
        if( Short.class == c || Boolean.TYPE == c) return Short.parseShort( value );
        if( Integer.class == c || Boolean.TYPE == c) return Integer.parseInt( value );
        if( Long.class == c || Boolean.TYPE == c) return Long.parseLong( value );
        if( Float.class == c || Boolean.TYPE == c) return Float.parseFloat( value );
        if( Double.class == c || Boolean.TYPE == c) return Double.parseDouble( value );
        return value;
    }

    @Override
    public T getEntity(Long id) {
        try {
            Constructor<T> ctor = clazz.getConstructor();
            Object obj = ctor.newInstance();
            List<Field> fields = getAllFields(new LinkedList<Field>(), clazz);
            String className = clazz.getName().substring(clazz.getName().lastIndexOf('.') + 1);
            String tableName = className;
            if (className.equals("Picture") || className.equals("Photo") ||className.equals("Drawing")) tableName = "picture";

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/JavaTest", "postgres", "12345");
            statement = connection.prepareStatement("SELECT * FROM public." + tableName + " WHERE ID = " + id.toString());
            ResultSet resultSet = statement.executeQuery();

            List<String> values = new ArrayList<String>();
            if (resultSet.next())
            {
                for (Field f : fields)
                {
                    values.add(resultSet.getString(f.getName().substring(f.getName().lastIndexOf('.') + 1)));
                }

                Method m;
                for (int i = 0; i < fields.size(); i++)
                {
                    Field f = fields.get(i);
                    m = clazz.getMethod("set" + f.getName().substring(0,1).toUpperCase() + f.getName().substring(1), f.getType());
                    m.invoke(obj, toObject(f.getType(), values.get(i)));
                }
                return (T)obj;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public List<T> getEntityList() {
        try {
            Constructor<T> ctor = clazz.getConstructor();
            List<Field> fields = getAllFields(new LinkedList<Field>(), clazz);
            String className = clazz.getName().substring(clazz.getName().lastIndexOf('.') + 1);
            String tableName = className;
            if (className.equals("Picture") || className.equals("Photo") ||className.equals("Drawing")) tableName = "picture";

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/JavaTest", "postgres", "12345");
            statement = connection.prepareStatement("SELECT * FROM public." + tableName);
            if (tableName.equals("picture"))
                statement = connection.prepareStatement("SELECT * FROM public." + tableName + " WHERE type = \'" + className + "\'" );
            ResultSet resultSet = statement.executeQuery();

            List<T> resultObjects = new ArrayList<T>();
            while (resultSet.next())
            {
                Object obj = ctor.newInstance();
                List<String> values = new ArrayList<String>();
                for (Field f : fields)
                {
                    values.add(resultSet.getString(f.getName().substring(f.getName().lastIndexOf('.') + 1)));
                }

                Method m;
                for (int i = 0; i < fields.size(); i++)
                {
                    Field f = fields.get(i);
                    m = clazz.getMethod("set" + f.getName().substring(0,1).toUpperCase() + f.getName().substring(1), f.getType());
                    m.invoke(obj, toObject(f.getType(), values.get(i)));
                }
                resultObjects.add((T)obj);
            }
            return resultObjects;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}
