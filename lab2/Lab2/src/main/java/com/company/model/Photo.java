package com.company.model;

import java.util.Objects;

public class Photo extends Picture {
    private String typeOfCamera;

    public Photo() {
    }

    public Photo(Long id, Long author, String theme, String type, Boolean nsfw, String typeOfCamera) {
        super(id, author, theme, type, nsfw);
        this.typeOfCamera = typeOfCamera;
    }

    public String getTypeOfCamera() {
        return typeOfCamera;
    }

    public void setTypeOfCamera(String typeOfCamera) {
        this.typeOfCamera = typeOfCamera;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Photo photo = (Photo) o;
        return Objects.equals(typeOfCamera, photo.typeOfCamera);
    }
}
