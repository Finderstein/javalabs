package com.company.model;

import java.util.Objects;

public class Drawing extends Picture {
    private String typeOfDrawing;

    public Drawing() {
    }

    public Drawing(Long id, Long author, String theme, String type, Boolean nsfw, String typeOfDrawing) {
        super(id, author, theme, type, nsfw);
        this.typeOfDrawing = typeOfDrawing;
    }

    public String getTypeOfDrawing() {
        return typeOfDrawing;
    }

    public void setTypeOfDrawing(String typeOfDrawing) {
        this.typeOfDrawing = typeOfDrawing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Drawing drawing = (Drawing) o;
        return Objects.equals(typeOfDrawing, drawing.typeOfDrawing);
    }
}
