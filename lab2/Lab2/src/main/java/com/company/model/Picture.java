package com.company.model;

public class Picture {
    private Long id;
    private Long author;
    private String theme;
    private String type;
    private Boolean nsfw;

    public Picture() {
    }

    public Picture(Long id, Long author, String theme, String type, Boolean nsfw) {
        this.id = id;
        this.author = author;
        this.theme = theme;
        this.type = type;
        this.nsfw = nsfw;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Boolean getNsfw() {
        return nsfw;
    }

    public void setNsfw(Boolean nsfw) {
        this.nsfw = nsfw;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Picture picture = (Picture) o;
        return id.equals(picture.id) &&
                author.equals(picture.author) &&
                theme.equals(picture.theme) &&
                type.equals(picture.type) &&
                nsfw.equals(picture.nsfw);
    }
}
