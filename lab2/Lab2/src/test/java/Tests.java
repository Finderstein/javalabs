import com.company.dao.*;
import com.company.model.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {
    DAO dao = new DAO();

    @Test
    public void testGetAuthor() {
        Author author = dao.getAuthor(1L);
        Author testAuthor = new Author(1L,"Error", "1", 17);
        assertFalse(author.equals(testAuthor));

        testAuthor.setName("Author1");
        assertEquals(author, testAuthor);
    }

    @Test
    public void testGetPicture() {
        Picture picture = dao.getPicture(1L);
        Picture testPicture = new Picture(1L,0L, "Sport", "Picture", false);
        assertNotEquals(picture, testPicture);

        testPicture.setAuthor(1L);
        assertEquals(picture, testPicture);
    }

    @Test
    public void testGetPhoto() {
        Photo photo = dao.getPhoto(4L);
        Photo testPhoto = new Photo(4L,0L, "Game", "Photo", true, "Super old camera");
        assertNotEquals(photo, testPhoto);

        testPhoto.setAuthor(1L);
        assertEquals(photo, testPhoto);
    }

    @Test
    public void testGetDrawing() {
        Drawing drawing = dao.getDrawing(7L);
        Drawing testDrawing = new Drawing(7L,0L, "Nature", "Drawing", true, "Watercolor");
        assertNotEquals(drawing, testDrawing);

        testDrawing.setAuthor(1L);
        assertEquals(drawing, testDrawing);
    }

    @Test
    public void testGetAuthorList() {
        List<Author> testAuthorList = new ArrayList<Author>();
        testAuthorList.add(new Author(1L,"Author1", "1", 17));
        testAuthorList.add(new Author(2L,"Author2", "2", 18));
        testAuthorList.add(new Author(3L,"Author3", "3", 19));

        List<Author> authorList = dao.getAuthorList();

        for(int i = 0; i < 3; i++)
        {
            assertEquals(testAuthorList.get(i), authorList.get(i));
        }
    }

    @Test
    public void testGetPictureList() {
        List<Picture> testPictureList = new ArrayList<Picture>();
        testPictureList.add(new Picture(1L,1L, "Sport", "Picture", false));
        testPictureList.add(new Picture(2L,2L, "Game", "Picture", true));
        testPictureList.add(new Picture(3L,3L, "Nature", "Picture", false));

        List<Picture> pictureList = dao.getPictureList();

        for(int i = 0; i < 3; i++)
        {
            assertEquals(testPictureList.get(i), pictureList.get(i));
        }
    }

    @Test
    public void testGetPhotoList() {
        List<Photo> testPhotoList = new ArrayList<Photo>();
        testPhotoList.add(new Photo(4L,1L, "Game", "Photo", true, "Super old camera"));
        testPhotoList.add(new Photo(5L,2L, "Sport", "Photo", false, "Super new camera"));
        testPhotoList.add(new Photo(6L,3L, "Nature", "Photo", true, "Normal camera"));

        List<Photo> photoList = dao.getPhotoList();

        for(int i = 0; i < 3; i++)
        {
            assertEquals(testPhotoList.get(i), photoList.get(i));
        }
    }

    @Test
    public void testGetDrawingList() {
        List<Drawing> testDrawingList = new ArrayList<Drawing>();
        testDrawingList.add(new Drawing(7L,1L, "Nature", "Drawing", true, "Watercolor"));
        testDrawingList.add(new Drawing(8L,2L, "Sport", "Drawing", false, "On tablet"));
        testDrawingList.add(new Drawing(9L,3L, "Game", "Drawing", false, "Wax"));

        List<Drawing> drawingList = dao.getDrawingList();

        for(int i = 0; i < 3; i++)
        {
            assertEquals(testDrawingList.get(i), drawingList.get(i));
        }
    }
}
